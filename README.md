# (Task 1) - Run terraform using Gitlab CI/CD

## Terraform

- [ ] Create the following unprotected variables in the Gitlab CI/CD :
  - student_name   
  - ip             
  - ssh_public_key 
  - access_key 
  - secret_key
All variables, that should be passed to Terraform, should start with TF_VAR_  (For example: TF_VAR_student_name="YOUR_NAME_HERE")

- [ ] Go to the Gitlab -> Bild -> Pipelines 
- Run the pipeline for the branch "main"
Pipeline should finish all jobs without error. 


# (Task 2) - Run terraform directly on the laptop

## Install Terraform on local machine:
- [ ] Depends of the OS on your laptop, you can install terraform in following way:
 ### MacOS
  - brew install terraform
 ### Ubuntu/Debian Linux
  - sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
  - wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
  - echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
  - sudo apt update && sudo apt-get install terraform
 ### Other Linux:
  - curl "https://releases.hashicorp.com/terraform/1.2.5/terraform_1.2.5_linux_amd64.zip" -o "terraform.zip" && unzip terraform.zip -d /usr/local/bin && chmod +x /usr/local/bin/terraform && rm -f terraform.zip
- Check is terraform installed correctly
```bash
terraform -version
```


## Run Terraform to create infrastructure in the AWS cloud:

- [ ] Init stage

```
terraform init
```
Terraform will download all modules, used in your project (in our case it is AWS module only)

- [ ] Validate stage

```
terraform validate
```
Terraform will validate syntax of terraform template file ( main.tf )

Check that there are no any errors in the terraform output

Output should looks like :
```
Success! The configuration is valid.
```

- [ ] plan stage

```
terraform plan
```
Terraform will create and show the plan of the required changes in the cloud , needed for create infrastructure

- [ ] Apply stage.

```
terraform apply
```
enter ssh key when it ask it (youre public ssh key stored in the ~/.ssh/id_rsa.pub file)

check the line
```
Plan: 2 to add, 0 to change, 0 to destroy.
```
answer yes and let terraform create infrasructure
